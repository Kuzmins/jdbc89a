import domain.Employee;
import repository.EmployeeRepository;
import repository.ProjectRepository;

import java.util.List;

public class MainAppRelationshipsExercises {

    public static void main(String[] args) {
        EmployeeRepository employeeRepository = new EmployeeRepository();
        ProjectRepository projectRepository = new ProjectRepository();

        System.out.println("\n\n------------ Display all projects--------");
        projectRepository.findAll().forEach(System.out::println);

        System.out.println("\n\n------------  Display all employees --------");
        employeeRepository.findAll().forEach(System.out::println);

        System.out.println("\n\n------------ Display all employees with names starting with the letter J--------");

        List<Employee> employees = employeeRepository.findByNameStartWith("J");
        employees.forEach(System.out::println);

        System.out.println("\n\n------------ Display all employees working in the Finance department --------");

        employees = employeeRepository.findByDepartmentName("Finance department");
        employees.forEach(System.out::println);

        System.out.println("\n\n------------ Display all employees alphabetically --------");
        employees = employeeRepository.findSortedByName();
        employees.forEach(System.out::println);
    }
}

import domain.Employee;
import domain.Project;
import utils.HibernateUtils;

public class MainAppHibernate2 {

    public static void main(String[] args) {


        HibernateUtils.doInTransaction(session -> {
            System.out.println("\n \n \n -------------- Find Employee --------------");

            Employee employee = session.find(Employee.class, 1);
            System.out.println(employee);

            Project project = session.find(Project.class, 1);
            System.out.println(project);
        });
    }
}

import domain.Department;
import domain.Employee;
import utils.HibernateUtils;

public class MainAppRelationships2 {

    public static void main(String[] args) {

        HibernateUtils.doInTransaction(session -> {
            Department department = new Department();
            department.setName("New awesome department");

            Employee employee = new Employee();
            employee.setFirstName("New employee");

            department.addEmployee(employee);

            session.save(department);
            session.save(employee);
        });
    }
}

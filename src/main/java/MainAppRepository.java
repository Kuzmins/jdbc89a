import domain.Department;
import repository.DepartmentHibernateRepository;

import java.util.List;

public class MainAppRepository {
    public static void main(String[] args) {
        DepartmentHibernateRepository departmentHibernateRepository = new DepartmentHibernateRepository();

        /* List all departments*/
        List<Department> allDepartments = departmentHibernateRepository.findAll();
        allDepartments.forEach(System.out::println);

        /* Create new department*/
        Department department = new Department();
        department.setName("Java guru");
        departmentHibernateRepository.createDepartment(department);
        System.out.println(department);

        /* List all departments new department should exist*/
       departmentHibernateRepository.findAll().forEach(System.out::println);

       /* Update newly created department */
        department.setName("Updated Department");
        departmentHibernateRepository.updateDepartment(department);

        System.out.println(department);

        /* Delete newly created department*/
        departmentHibernateRepository.deleteDepartment(department);

        /* List all departments newly created department should not exist*/
        departmentHibernateRepository.findAll().forEach(System.out::println);
    }
}

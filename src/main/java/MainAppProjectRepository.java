import domain.Project;
import repository.ProjectRepository;

import java.util.List;

public class MainAppProjectRepository {

    public static void main(String[] args) {
        ProjectRepository projectRepository = new ProjectRepository();

        /* List all projects*/
        List<Project> allProjects = projectRepository.findAll();
        allProjects.forEach(System.out::println);

        /* Create new project*/
        Project project = new Project();
        project.setDescription("New project");
        projectRepository.createProject(project);

        /* List all projects new project should exist*/
        projectRepository.findAll().forEach(System.out::println);

        /* Update newly created project */
        project.setDescription("Updated description");
        projectRepository.updateProject(project);

        System.out.println(project);

        /* Delete newly created project*/
        projectRepository.deleteProject(project);

        /* List all projects newly created project should not exist*/
        projectRepository.findAll().forEach(System.out::println);
    }
}

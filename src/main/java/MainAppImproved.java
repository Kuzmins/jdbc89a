import java.util.List;

import domain.Department;
import repository.DepartmentRepository;

public class MainAppImproved {

    public static void main(String[] args) {

        DepartmentRepository departmentRepository = new DepartmentRepository();

        Department department = departmentRepository.findById(2);
        System.out.println("Departments before update:");
        System.out.println(department);

        departmentRepository.updateDepartment(2, "'Select *'");

        Department departmentAfterUpdate = departmentRepository.findById(2);
        System.out.println(departmentAfterUpdate);


       /* System.out.println("Departments before update:");
        allDepartments.forEach(it-> System.out.println(it));

        departmentRepository.updateDepartment("HR new 2");

        allDepartments = departmentRepository.findAll();
        System.out.println("Departments after update:");
        allDepartments.forEach(it-> System.out.println(it));


        Department department = new Department(5, "Our test department");
        departmentRepository.save(department);

        allDepartments = departmentRepository.findAll();
        System.out.println("Departments after adding:");
        allDepartments.forEach(it-> System.out.println(it));

*/

    }
}

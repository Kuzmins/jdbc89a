import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

import com.mysql.cj.result.SqlDateValueFactory;

public class MainApp {

    public static final String DATABASE_HOST = "jdbc:mysql://192.168.99.101:3306/jdbcJava89aHumanResources?serverTimezone=UTC";

    public static final String DATABASE_USERNAME = "root";

    public static final String DATABASE_PASSWORD = "example";

    public static void main(String[] args) {

        try {
            Connection conn = DriverManager.getConnection(DATABASE_HOST, DATABASE_USERNAME, DATABASE_PASSWORD);
            System.out.println(conn.getAutoCommit());
            conn.setAutoCommit(false);

            conn.commit();

            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM departments");
          //  ResultSetMetaData rsmd = rs.getMetaData();
           // String firstColumnName = rsmd.getColumnName(1);

            while (rs.next()) {
                Integer deptId = rs.getInt("departmentId");
                String deptName = rs.getString("name");
                System.out.println(deptId + " " + deptName);
            }

            System.out.println("Display all projects: ");
            rs = stmt.executeQuery("SELECT * FROM projects");
            while (rs.next()) {
                Integer projectId = rs.getInt("projectId");
                String description = rs.getString("description");
                System.out.println(projectId + " " + description);
            }

            System.out.println(" \nDisplay all employees: ");
            rs = stmt.executeQuery("SELECT * FROM employees");
            printEmployees(rs);

            System.out.println(" \n Display all employees with names starting with the letter J: ");
            rs = stmt.executeQuery("SELECT * FROM employees where firstName like 'J%'");
            printEmployees(rs);

            System.out.println(" \n Display all employees that haven’t been assigned to a department: ");

            rs = stmt.executeQuery("SELECT * FROM employees where departmentId is null");
            printEmployees(rs);

            System.out.println(" \n Display all employees along with the department they’re in:  ");
            rs = stmt.executeQuery("SELECT e.employeeId, e.firstName, e.lastName, e.departmentId, e.dateOfBirth, d.name"
                + " from employees e JOIN departments d ON e.departmentId = d.departmentId");

            printEmployees(rs);

            rs.close();
            stmt.close();
            conn.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void printEmployees(ResultSet rs) throws SQLException {
        while (rs.next()) {
            String departmentName = "";

            Integer employeeId = rs.getInt("employeeId");
            String firstName = rs.getString("firstName");
            String lastName = rs.getString("lastName");
            Date dateOfBirth = rs.getDate("dateOfBirth");
            Integer departmentId = rs.getInt("departmentId");
            try {
                departmentName = rs.getString("name");
            } catch (SQLException ex) {
            }
            System.out.println(employeeId + " " + firstName + " " + lastName + " " + dateOfBirth + " " + departmentId + " " + departmentName);
        }
    }

}

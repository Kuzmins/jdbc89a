import domain.Department;
import domain.Employee;
import domain.Project;
import org.hibernate.Session;
import org.hibernate.Transaction;
import utils.HibernateUtils;

import java.util.Set;

public class MainAppRelationships {

    public static void main(String[] args) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        Employee employee = session.find(Employee.class, 1);
        System.out.println(employee);

        System.out.println("----------------------------------------");

        System.out.println(employee.getDepartment());

        transaction.commit();
        session.close();

        System.out.println("---------------------------------------");
        session = HibernateUtils.getSessionFactory().openSession();
        transaction = session.beginTransaction();

        Department department = session.find(Department.class, 25);
        System.out.println("----------------------");
        Set<Employee> employees = department.getEmployees();
        employees.forEach(System.out::println);

        transaction.commit();
        session.close();

        System.out.println("\n\n-----------------------------------------------------------");

        session = HibernateUtils.getSessionFactory().openSession();
        transaction = session.beginTransaction();

        Project project = session.find(Project.class, 1);

        project.getEmployees().forEach(System.out::println);

        transaction.commit();
        session.close();
    }
}

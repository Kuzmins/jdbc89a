import domain.Employee;
import repository.EmployeeHibernateRepository;
import repository.EmployeeRepository;

import java.util.List;

public class MainAppEmployeeRepository2 {

    public static void main(String[] args) {
        EmployeeRepository employeeHibernateRepository = new EmployeeRepository();

        /* List all employees*/
        List<Employee> allEmployees = employeeHibernateRepository.findAll();
        allEmployees.forEach(System.out::println);

        /* Create new employee */
        Employee employee = new Employee();
        employee.setFirstName("New name");
        employeeHibernateRepository.create(employee);
        System.out.println(employee);

        /* Update employee*/
        employee.setFirstName("Updated Name");
        employeeHibernateRepository.update(employee);
        Employee employeeFromDb = employeeHibernateRepository.findById(employee.getId());
        System.out.println(employeeFromDb);

        /* Delete employee */
        employeeHibernateRepository.delete(employeeFromDb);

        /* List all employees*/
        employeeHibernateRepository.findAll().forEach(System.out::println);
    }
}

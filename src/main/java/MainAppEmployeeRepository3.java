import domain.Department;
import domain.Employee;
import utils.HibernateUtils;

import java.util.Set;

public class MainAppEmployeeRepository3 {

    public static void main(String[] args) {
        HibernateUtils.doInTransaction(session -> {
            Department department = session.find(Department.class, 1);

            Set<Employee> employees = department.getEmployees();

            Department departmentToTransfer = session.find(Department.class, 25);

            for (Employee employee : employees) {
                departmentToTransfer.addEmployee(employee);
            }

            session.delete(department);
        });
    }
}

import domain.Department;
import org.hibernate.Session;
import utils.HibernateUtils;

public class MainAppHibernate {

    public static void main(String[] args) {

        Department newDepartment = HibernateUtils.doInTransactionWithResult(session -> {
            Department department = new Department();
            department.setName("New department");
            session.save(department);
            System.out.println(department);
            return department;
        });

        HibernateUtils.doInTransaction(session -> {
            Department department = session.find(Department.class, newDepartment.getDepartmentId());
            department.setName("Update");
        });

        HibernateUtils.doInTransaction(session -> {
            session.delete(newDepartment);
        });
    }

    static class DbActionImpl implements HibernateUtils.DbActionWithoutResult {

        @Override
        public void execute(Session session) {
            System.out.println("Inside db Action");
        }
    }
}

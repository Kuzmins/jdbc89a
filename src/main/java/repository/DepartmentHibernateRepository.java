package repository;

import domain.Department;
import utils.HibernateUtils;

import java.util.List;

public class DepartmentHibernateRepository {

    public Department findById(Integer id) {
        return HibernateUtils.doInTransactionWithResult(session -> {
            Department department = session.find(Department.class, id);
            return department;
        });
    }

    public void updateDepartment(Department department) {
        HibernateUtils.doInTransaction(session -> {
            session.update(department);
        });
    }

    public void deleteDepartment(Department department) {
        HibernateUtils.doInTransaction(session -> {
            session.delete(department);
        });
    }

    public Department createDepartment(Department department) {
        if (department.getDepartmentId() != null) {
            throw new IllegalArgumentException("Department already saved!");
        }

        return HibernateUtils.doInTransactionWithResult(session -> {
            session.save(department);
            return department;
        });
    }

    public List<Department> findAll() {
        return HibernateUtils.doInTransactionWithResult(session -> {
            return session.createQuery("from Department", Department.class).list();
        });
    }
}

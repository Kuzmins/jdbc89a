package repository;

import domain.Employee;
import utils.HibernateUtils;

import java.util.List;

public class EmployeeHibernateRepository {
    public Employee findById(Integer id) {
        return HibernateUtils.doInTransactionWithResult(session -> {
            Employee employee = session.find(Employee.class, id);
            return employee;
        });
    }
    public void updateEmployee(Employee employee) {
        HibernateUtils.doInTransaction(session -> {
            session.update(employee);
        });
    }
    public void deleteEmployee(Employee employee) {
        HibernateUtils.doInTransaction(session -> {
            session.delete(employee);
        });
    }
    public Employee createEmployee(Employee employee) {
        if (employee.getId() != null) {
            throw new IllegalArgumentException("Employee already saved!");
        }
        return HibernateUtils.doInTransactionWithResult(session -> {
            session.save(employee);
            return employee;
        });
    }
    public List<Employee> findAll() {
        return HibernateUtils.doInTransactionWithResult(session -> {
            return session.createQuery("from Employee", Employee.class).list();
        });
    }
}

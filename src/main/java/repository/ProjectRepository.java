package repository;

import domain.Department;
import domain.Project;
import utils.HibernateUtils;

import java.util.List;

public class ProjectRepository {
    public Project findById(Integer id) {
        return HibernateUtils.doInTransactionWithResult(session -> {
            Project project = session.find(Project.class, id);
            return project;
        });
    }

    public void updateProject(Project project) {
        HibernateUtils.doInTransaction(session -> {
            session.update(project);
        });
    }

    public void deleteProject(Project project) {
        HibernateUtils.doInTransaction(session -> {
            session.delete(project);
        });
    }

    public Project createProject(Project project) {
        if (project.getId() != null) {
            throw new IllegalArgumentException("Project already saved!");
        }

        return HibernateUtils.doInTransactionWithResult(session -> {
            session.save(project);
            return project;
        });
    }

    public List<Project> findAll() {
        return HibernateUtils.doInTransactionWithResult(session -> {
            return session.createQuery("from Project", Project.class).list();
        });
    }
}

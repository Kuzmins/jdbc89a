package repository;

import java.util.List;

public interface HibernateRepository<R, ID> {
    void create(R domain);
    void update(R domain);
    List<R> findAll();
    R findById(ID id);
    void delete(R domain);
}

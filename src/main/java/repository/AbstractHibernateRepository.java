package repository;

import utils.HibernateUtils;

import java.util.List;

public abstract class AbstractHibernateRepository<R, ID> implements HibernateRepository<R, ID>{

    abstract Class<R> getDomainClass();
    @Override
    public void create(R domain) {
        HibernateUtils.doInTransaction(session -> {
            session.save(domain);
        });
    }

    @Override
    public void update(R domain) {
        HibernateUtils.doInTransaction(session -> {
            session.update(domain);
        });
    }

    @Override
    public List<R> findAll() {
        return HibernateUtils.doInTransactionWithResult(session -> {
            String simpleName = getDomainClass().getSimpleName();
            return session.createQuery("from " + simpleName, getDomainClass()).list();
        });
    }

    @Override
    public R findById(ID id) {
       return HibernateUtils.doInTransactionWithResult(session -> session.find(getDomainClass(), id));
    }


    @Override
    public void delete(R domain) {
        HibernateUtils.doInTransaction(session -> {
            session.delete(domain);
        });
    }

}

package repository;

import domain.Employee;
import utils.HibernateUtils;

import java.util.List;

public class EmployeeRepository extends AbstractHibernateRepository<Employee, Integer> {

    @Override
    Class<Employee> getDomainClass() {
        return Employee.class;
    }

    public List<Employee> findByNameStartWith(String characters) {
        return HibernateUtils.doInTransactionWithResult(session -> {

            String hql = "from Employee e where e.firstName like :name";
            return session.createQuery(hql).setParameter("name", characters + "%").list();
        });
    }

    public List<Employee> findByDepartmentName(String departmentName) {
        return HibernateUtils.doInTransactionWithResult(session -> {

            String hql = "from Employee e where e.department.name = :name";
            return session.createQuery(hql).setParameter("name", departmentName).list();
        });
    }

    public List<Employee> findSortedByName() {
        return HibernateUtils.doInTransactionWithResult(session -> {

            String hql = "from Employee e order by e.firstName asc";
            return session.createQuery(hql).list();
        });
    }

}
